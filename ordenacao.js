

//Entrada de dados----------------------------------------------------------------------------------------------------------
/*function ordena(opcao){
//var vetorB = [];
var entrada;
var tamanho;

entrada = parseInt(prompt("Informe um numero"));

do{
//vetorB.push(entrada); // insere no vetorB
//entrada = parseInt(prompt("Informe outro numero. -1 p/ encerrar"));
}

while (entrada != -1);
switch(opcao){
case 1: bubbleSort();
break;
case 2: quicksort(vetorB, 0, vetorB.length-1);
break;
case 3: selecao_direta(vetorB);
break;
case 4: insercao_direta(vetorB);
break;

}
alert("vetorB ordenado " + vetorB);
}
*/

var vetor = [];
var vetorB = [];

//preenche automático o vetor
function preencheVetor(){
    var tamanho = prompt("digite o tamanho do vetorB");
    //var vetor = [];
    var cont;
    for (cont = 0; cont < tamanho; cont++){
        vetor.push(Math.floor(Math.random()* tamanho) + 1);
    }

    return vetor;

    //document.write(vetor);
}

//Cria um novo vetor para ser usado nas funções, sem alterar o vetor original, preenchido automaticamente
function pegaVetor(){
    vetorB = vetor;
    return vetorB;
}

//1.BubbleSort---------------------------------------------------------------------------------------------------------------
function testaBubbleSort(){ //
    pegaVetor();
    bubbleSort();
}

function bubbleSort(){
    // pegando o tempo inicial quando executa a função
    let timeStart = performance.now();

    var troca, i, j;
    for(i=0;i<vetorB.length;i++){
        for(j=0;j<vetorB.length-1;j++){
            if (vetorB[j] > vetorB[j+1]){
                troca = vetorB[j];
                vetorB[j] = vetorB[j+1];
                vetorB[j+1] = troca;
            }
        }
    }
    //document.write("\nBubbleSort:\n" + vetorB);
    document.getElementById('card-bubbleSort').innerHTML = vetorB;
    // pegando o tempo final quando executa a função
    let timeEnd = performance.now();
    document.getElementById('time-bubble').innerHTML = `Runtime: ${timeEnd - timeStart}`;
    // console.log(`Duração ${timeEnd - timeStart}`);
}



//---------------------------------------------------------------------------------------------------------------------------

//2.QuickSort----------------------------------------------------------------------------------------------------------------
function testaQuickSort(){
    particionador();
    quickSort()
    pegaVetor();
}

//quicksort
function particionador(vetorB, esq, dir) {
    let pivo   = vetorB[Math.floor((dir + esq) / 2)];
    let i = esq;
    let j = dir;

    while (i <= j) {
        while (vetorB[i] < pivo) {
            i++;
        }
        while (vetorB[j] > pivo) {
            j--;
        }
        if (i <= j) {
            troca(vetorB, i, j);
            i++;
            j--;
        }
    }
    return i;
}


let timeStart = performance.now();
console.log(timeStart);
function quickSort(vetorB, esq, dir) {
    var vetorB;
    var index;
    if (vetorB.length > 1) {
        esq = typeof esq != "number" ? 0 : esq;
        dir = typeof dir != "number" ? vetorB.length - 1 : dir;
        index = particionador(vetorB, esq, dir);
        if (esq < index - 1) {
            quickSort(vetorB, esq, index - 1);
        }
        if (index < dir) {
            quickSort(vetorB, index, dir);
        }
    }

    return vetorB;


}
// pega o tempo final da excução da função
let timeEnd = performance.now();

document.getElementById('time-quick').innerHTML = `Runtime: ${timeEnd - timeStart}`;
let a = preencheVetor();
document.getElementById('card-quickSort').innerHTML = quickSort(a)

// function quickSort(vetorB, esq, dir){
//     console.log(vetorB);
//     let timeStart = performance.now();
//     var pivo = esq;
//     var i,j, aux;
//     for(i=esq+1;i<=dir;i++){
//         j = i;
//         if(vetorB[j] < vetorB[pivo]){
//             aux = vetorB[j];
//             while(j > pivo){
//                 vetorB[j] = vetorB[j-1];
//                 j--;
//             }
//             vetorB[j] = aux;
//             pivo++;
//         }
//     }
//     if(pivo-1 >= esq){
//         quicksort(vetorB,esq,pivo-1);
//     }
//     if(pivo+1 <= dir){
//         quicksort(vetorB,pivo+1,dir);
//     }
//
//
//     //document.write("\nBubbleSort:\n" + vetorB);
//     document.getElementById('card-quickSort').innerHTML = vetorB;
//     // pegando o tempo final quando executa a função
//     let timeEnd = performance.now();
//     document.getElementById('time-quick').innerHTML = `Tempo de execução: ${timeEnd - timeStart}`;
//     // console.log(`Duração ${timeEnd - timeStart}`);
//
// }
// let b = [1,1,2,3,4,5,1,1,1,1]
// console.log(`>>>>>>>>${quickSort(b)}`);
//---------------------------------------------------------------------------------------------------------------------------

// Função para troca de elementos em um vetor
function troca(vetorB, fIndex, sIndex){
    let temp = vetorB[fIndex];
    vetorB[fIndex] = vetorB[sIndex];
    vetorB[sIndex] = temp;
}

//3.Selecao Direta ---------------------------------------------------------------------------------------------------------------
function testaSelecaoDireta(){
    pegaVetor();
    selecaoDireta();
}

function selecaoDireta(){
    let timeStart = performance.now();
    var len = vetorB.length;
    console.log(vetorB);
    var min;

    for (i=0; i < len; i++){
        //set minimum to this position
        min = i;
        //check the rest of the array to see if anything is smaller
        for (j=i+1; j < len; j++){
            if (vetorB[j] < vetorB[min]){
                min = j;
            }
        }

        //if the minimum isn't in the position, swap it
        if (i != min){
            troca(vetorB, i, min);
        }
    }
    //console.log(`Selection Sort >>>> ${vetorB}`);
    document.getElementById('card-selecaoDireta').innerHTML = vetorB;
    let timeEnd = performance.now();
    document.getElementById('time-selection').innerHTML = `Runtime: ${timeEnd - timeStart}`;

}
//---------------------------------------------------------------------------------------------------------------------------

//4.Insercao Direta--------------------------------------------------------------------------------------------------------
function testaInsercaoDireta(){
    pegaVetor();
    insercaoDireta();
}

function insercaoDireta(){
    let timeStart = performance.now();
    var i, j, aux;

    for (i = 1; i < vetorB.length; i++){
        aux = vetorB[i];
        j = i;

        while ((j > 0) && (vetorB[j-1] > aux)){
            vetorB[j] = vetorB[j-1];
            j -= 1;
        }
        vetorB[j] = aux;
    }

    document.getElementById('card-insercaoDireta').innerHTML = vetorB;
    let timeEnd = performance.now();
    document.getElementById('time-inserction').innerHTML = `Runtime: ${timeEnd - timeStart}`;
}
//---------------------------------------------------------------------------------------------------------------------------

//preenchevetorB();
//ordena();
